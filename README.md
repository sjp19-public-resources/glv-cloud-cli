# Galvanize Cloud CLI

This tool is a command line interface to manage applications
deployed to Galvanize Cloud.

## How to use

Run the following command:

```sh
$ docker run glv-cloud-cli-img:latest help

    A command line interface to Deploy, Delete, and List
                    applications running in your teams environment.
                    Documentation at: https://gitlab.com/sjp19-public-resources/glv-cloud-cli/-/wikis/home

    Usage:
      glv-cloud-cli [command]

    Available Commands:
      completion  Generate the autocompletion script for the specified shell
      delete      delete applications on Galvanize Cloud
      deploy      Deploy applications to Galvanize Cloud
      help        Help about any command
      list        list applications on Galvanize Cloud
      login       login to Galvanize Cloud
      registry    Add registry to Galvanize Cloud

    Flags:
      -a, --author string    author name for copyright attribution (default "Galvanize Inc.")
          --config string    config file (default is $HOME/.glv.yaml)
      -h, --help             help for glv-cloud-cli
      -l, --license string   name of license for the project
          --viper            use Viper for configuration (default true)

    Use "glv-cloud-cli [command] --help" for more information about a command.
```
