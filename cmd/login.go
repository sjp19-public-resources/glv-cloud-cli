package cmd

import (
	"fmt"

	"glv-cloud-cli/auth"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func init() {
	rootCmd.AddCommand(loginCmd)
}

var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "login to Galvanize Cloud",
	Long:  `login to Galvanize Cloud`,
	Run: func(cmd *cobra.Command, args []string) {
		token = auth.FetchUserToken().AccessToken
		fmt.Println("Token", token)
		viper.Set("Token", token)

		// Save changes to file
		err := viper.WriteConfig()
		if err != nil {
			panic(fmt.Errorf("failed to write config file: %w", err))
		}
	},
}
