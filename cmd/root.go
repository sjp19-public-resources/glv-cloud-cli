package cmd

import (
	"fmt"
	"io"
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	// Used for flags.
	cfgFile     string
	userLicense string
	token       string
	appName     string
	verbose     bool

	rootCmd = &cobra.Command{
		Use:   "glv-cloud-cli",
		Short: "glv-cloud-cli manages your Galvanize Cloud environment",
		Long: `A command line interface to Deploy, Delete, and List
		applications running in your teams environment.
		Documentation at: https://gitlab.com/sjp19-public-resources/glv-cloud-cli/-/wikis/home`,
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.glv.yaml)")
	rootCmd.PersistentFlags().StringVarP(&token, "token", "t", "", "token for glv-cloud")
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Verbosity")

	viper.BindPFlag("author", rootCmd.PersistentFlags().Lookup("author"))
	viper.BindPFlag("useViper", rootCmd.PersistentFlags().Lookup("viper"))
	viper.SetDefault("author", "Galvanize Inc. <foss@galvanize.com>")
	viper.SetDefault("license", "MIT")
}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".cobra" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".glv")
		if err := viper.ReadInConfig(); err != nil {
			if _, ok := err.(viper.ConfigFileNotFoundError); ok {
				// Config file not found
				rootCmd.MarkPersistentFlagRequired("token")
			} else {
				// Config file was found but another error was produced
				log.Fatal("Config file is corrupt. Please correct or remove ~/.glv.yaml")
			}
		}

		token = viper.GetString("token")
		if token == "" {
			rootCmd.MarkPersistentFlagRequired("token")
		}
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

	if verbose {
		log.SetOutput(os.Stdout)
	} else {
		log.SetOutput(io.Discard)
	}
}
