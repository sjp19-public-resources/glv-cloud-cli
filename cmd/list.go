package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"glv-cloud-cli/api"
	constants "glv-cloud-cli/constants"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(listCmd)
}

func getList() api.ListApi {
	// Define request
	req, err := http.NewRequest("Get",
		constants.GlvCloudV1+constants.ListEndpoint, nil)
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	// Read response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	var list api.ListApi
	err = json.Unmarshal(body, &list)
	if err != nil {
		panic(err)
	}

	return list
}

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "list applications on Galvanize Cloud",
	Long:  `list applications on Galvanize Cloud`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("list applications...")
		list := getList()

		// Marshal the User struct to JSON
		jsonBytes, err := json.MarshalIndent(list, "", "    ")
		if err != nil {
			panic(err)
		}

		// Print the JSON string
		fmt.Println(string(jsonBytes))
	},
}
