package cmd

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"

	"glv-cloud-cli/api"
	constants "glv-cloud-cli/constants"

	"github.com/spf13/cobra"
)

var (
	imageName            string
	environmentVariables []string
	port                 uint16
	expose               bool
	deleteDeployment     bool
)

func init() {
	deployCmd.Flags().StringVarP(&imageName, "image", "i", "",
		"Fully qualified image name. e.g. registry.gitlab.com/path/to/image:latest")

	deployCmd.Flags().StringVarP(&appName, "app", "a", "",
		"Name of application.")
	deployCmd.MarkFlagRequired("app")

	deployCmd.Flags().Uint16VarP(&port, "port", "p", uint16(80),
		"Port to expose (Defaults to 80).")

	deployCmd.Flags().BoolVarP(&expose, "expose", "x", true,
		"Expose to web.")
	deployCmd.MarkFlagsMutuallyExclusive("expose", "port")

	deployCmd.Flags().StringSliceVarP(&environmentVariables, "environment", "e",
		[]string{}, "Environ variables in the form VAR=VAL. This may be repeated.")

	deployCmd.Flags().BoolVarP(&deleteDeployment, "delete", "d", false, "Delete deployment.")

	rootCmd.AddCommand(deployCmd)
}

func deleteDeploymentFn() {
	fmt.Printf("Deleting Deployment %s...\n", appName)

	deploymentRef := api.DeploymentRef{
		AppName: appName,
	}

	// Encode the struct as JSON
	deploymentJson, err := json.Marshal(deploymentRef)
	if err != nil {
		panic(err)
	}

	// Define request
	req, err := http.NewRequest("DELETE",
		constants.GlvCloudV1+constants.DeployEndpoint,
		bytes.NewBuffer(deploymentJson))
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	// Read response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	log.Println(string(body))
}

func createDeploymentFn() {
	fmt.Printf("Creating Deployment %s...\n", appName)

	deployment := api.Deployment{
		DeploymentRef: api.DeploymentRef{
			AppName: appName,
		},
		ImageUri: imageName,
		Port:     port,
		Expose:   expose,
		Environ:  environmentVariables,
	}

	// Encode the struct as JSON
	deploymentJson, err := json.Marshal(deployment)
	if err != nil {
		panic(err)
	}

	// Define request
	req, err := http.NewRequest("POST",
		constants.GlvCloudV1+constants.DeployEndpoint,
		bytes.NewBuffer(deploymentJson))
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	// Debug
	log.Println(deployment)

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	// Read response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	log.Println(string(body))
}

var deployCmd = &cobra.Command{
	Use:   "deploy",
	Short: "Deploy applications to Galvanize Cloud",
	Long:  `Deploy applications to Galvanize Cloud`,
	RunE: func(cmd *cobra.Command, args []string) error {

		fmt.Printf("Deploying application %s...\n", appName)

		//Debug
		log.Print("----\n")
		log.Println("App: ", appName)
		log.Println("Image: ", imageName)
		log.Println("Port: ", port)
		log.Println("Expose: ", expose)
		log.Println("Delete: ", deleteDeployment)
		log.Println("Token", token)
		log.Print("----\n")

		if imageName == "" && deleteDeployment == false {
			return errors.New("either --image or --delete is required")
		}

		for _, v := range environmentVariables {
			log.Println("Environment variable: ", v)
			match, _ := regexp.MatchString("^\\w+\\=[\\w\\?\\:\\/\\.\\=\\;\\\\]+$", v)
			if !match {
				log.Fatal("Environment variables must in the format VARIABLE=VALUE")
			}
		}

		match, _ := regexp.MatchString("^[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*$", appName)
		if !match {
			log.Fatal("Application name must contain characters a-z, A-Z, 0-9, and non-leading or trailing hyphen")
		}

		if deleteDeployment {
			deleteDeploymentFn()
		} else {
			createDeploymentFn()
		}

		return nil
	},
}
