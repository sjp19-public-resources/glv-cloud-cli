package cmd

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	api "glv-cloud-cli/api"
	constants "glv-cloud-cli/constants"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/spf13/cobra"
)

var (
	registryServer   string
	registryUsername string
	registryPassword string
	registryEmail    string
	deleteRegistry   bool
)

func init() {

	registryCmd.Flags().StringVar(&registryServer, "server", "",
		"URL of registry server")
	registryCmd.MarkFlagRequired("server")

	registryCmd.Flags().StringVar(&registryUsername, "username", "",
		"Username of registry account")

	registryCmd.Flags().StringVar(&registryPassword, "password", "",
		"Password of registry account")

	registryCmd.Flags().StringVar(&registryEmail, "email", "",
		"Email of registry account")

	registryCmd.Flags().BoolVarP(&deleteRegistry, "delete", "d", false, "delete registry")
	registryCmd.MarkFlagsRequiredTogether("username", "password", "email")

	rootCmd.AddCommand(registryCmd)
}

func createRegistryFn() {
	fmt.Printf("Creating Registry %s...\n", registryServer)
	registry := api.Registry{
		RegistryRef: api.RegistryRef{
			Server: registryServer},
		Username: registryUsername,
		Password: registryPassword,
		Email:    registryEmail,
	}

	// Encode the struct as JSON
	RegistryJson, err := json.Marshal(registry)
	if err != nil {
		panic(err)
	}

	// Define request
	req, err := http.NewRequest("POST",
		constants.GlvCloudV1+constants.RegistryEndpoint,
		bytes.NewBuffer(RegistryJson))
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	// Debug
	log.Println("RegistryJson:", string(RegistryJson))

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	// Read response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	log.Println(string(body))
	fmt.Println("OK.")
}

func deleteRegistryFn() {
	fmt.Printf("Deleting Registry %s...\n", registryServer)
	registry := api.RegistryRef{
		Server: registryServer,
	}

	// Encode the struct as JSON
	RegistryJson, err := json.Marshal(registry)
	if err != nil {
		panic(err)
	}

	// Define request
	req, err := http.NewRequest("DELETE",
		constants.GlvCloudV1+constants.RegistryEndpoint,
		bytes.NewBuffer(RegistryJson))
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	// Debug
	log.Println("RegistryJson:", string(RegistryJson))

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	// Read response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	log.Println(string(body))
	fmt.Println("OK.")
}

var registryCmd = &cobra.Command{
	Use:   "registry",
	Short: "Add registry to Galvanize Cloud",
	Long:  `Add registry to Galvanize Cloud`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if registryUsername == "" && deleteRegistry == false {
			return errors.New("either --username,--password,--email or --delete is required")
		}

		log.Println("----VARIABLES----")
		log.Println("registry applications...")
		log.Println("Server: ", registryServer)
		log.Println("Username: ", registryUsername)
		log.Println("Password: ", registryPassword)
		log.Println("Email: ", registryEmail)
		log.Println("Delete registry", deleteRegistry)
		log.Print("----")

		if deleteRegistry {
			deleteRegistryFn()
		} else {
			createRegistryFn()
		}
		return nil
	},
}
