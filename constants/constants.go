package constants

const (
	GlvCloudUri      string = "http://glv-cloud.mod3projects.com"
	GlvCloudApiV1    string = "/api/v1"
	GlvCloudV1       string = GlvCloudUri + "/api/v1"
	DeployEndpoint   string = "/deploy"
	ListEndpoint     string = "/list"
	RegistryEndpoint string = "/registry"
)
