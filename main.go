package main

import (
	"glv-cloud-cli/cmd"
)

func main() {
	cmd.Execute()
}
