package api

type DeploymentRef struct {
	AppName string `json:"appName"`
}

type Deployment struct {
	DeploymentRef          // AppName is promoted to Deployment
	ImageUri      string   `json:"imageUri"`
	Port          uint16   `json:"port"`
	Expose        bool     `json:"expose"`
	Environ       []string `json:"environ"`
}
