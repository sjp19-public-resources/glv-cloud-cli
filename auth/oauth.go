package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strings"

	api "glv-cloud-cli/api"

	"github.com/pkg/browser"
)

func FetchUserToken() api.AuthResponse {
	const (
		redirectURL     = "https://localhost:4321"
		glvAuthorizeURL = "https://galvanize-auth-staging.herokuapp.com/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=%s&scope=%s&state=%s"
		clientID        = "VeMb_J7hgk3nc3IAIf9rHiM2s5qazcFBSFxGecrY93I"
		clientSecret    = "HRd4hM1-chsgOjsfS4l0w4zeKSfEDCWgUrkCaZcSDjc"
		responseType    = "code"
	)

	// authorization code - received in callback
	code := ""
	// local state parameter for cross-site request forgery prevention
	state := fmt.Sprint(rand.Int())
	// scope of the access: we want to modify user's playlists
	scope := ""
	// loginURL
	path := fmt.Sprintf(glvAuthorizeURL, clientID, redirectURL, responseType, scope, state)

	// channel for signaling that server shutdown can be done
	messages := make(chan bool)

	// callback handler, redirect from authentication is handled here
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// check that the state parameter matches
		if s, ok := r.URL.Query()["state"]; ok && s[0] == state {
			// code is received as query parameter
			if codes, ok := r.URL.Query()["code"]; ok && len(codes) == 1 {
				// save code and signal shutdown
				code = codes[0]
				messages <- true
			}
		}
		// redirect user's browser to Galvanize home page
		http.Redirect(w, r, "https://www.galvanize.com/", http.StatusSeeOther)
	})

	fmt.Println("Opening login page...")
	// open user's browser to login page
	if err := browser.OpenURL(path); err != nil {
		panic(fmt.Errorf("failed to open browser for authentication %s", err.Error()))
	}

	fmt.Println("Waiting for login...")
	server := &http.Server{Addr: ":4321"}
	// go routine for shutting down the server
	go func() {
		okToClose := <-messages
		if okToClose {
			if err := server.Shutdown(context.Background()); err != nil {
				log.Fatal("Failed to shutdown server", err)
			}
		}
	}()
	// start listening for callback - we don't continue until server is shut down
	log.Println(server.ListenAndServeTLS("localhost.crt", "localhost.key"))

	// Define query parameters
	data := url.Values{}
	data.Set("grant_type", "authorization_code")
	data.Set("code", code)
	data.Set("client_id", clientID)
	data.Set("client_secret", clientSecret)
	data.Set("redirect_uri", redirectURL)
	// Scope is defined externally. Changing this will not grant more access.
	data.Set("scope", "")
	payload := strings.NewReader(data.Encode())

	// Define request
	req, err := http.NewRequest("POST",
		"https://galvanize-auth-staging.herokuapp.com/oauth/token", payload)
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	// Read response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	// Read response headers
	// for key, value := range resp.Header {
	// 	fmt.Printf("%s: %s\n", key, value)
	// }

	var authResponse api.AuthResponse
	err = json.Unmarshal(body, &authResponse)
	if err != nil {
		panic(err)
	}

	return authResponse
}
